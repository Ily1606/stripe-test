<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Payment</title>
  <link rel="stylesheet" href="./assets/css/index.css">
  <script src="https://js.stripe.com/v3/"></script>
</head>
<body>
  <div class="container max-w-2xl mx-auto my-20 p-6 rounded-lg border shadow-lg">
    <h1 class="text-lg font-bold text-gray-800 mb-6">Contact information</h1>
    <div id="payment-contact-info"></div>
    <h1 class="text-lg font-bold text-gray-800 my-6">Payment information</h1>
    <div id="payment-request-button">
      <!-- A Stripe Element will be inserted here. -->
    </div>
    <div class="flex gap-x-4 my-2">
      <a class="bg-gray-600 text-white py-2 px-4 rounded-md text-center block" href="/">Back</a>
      <button class="bg-blue-500 px-4 py-2 text-white font-medium min-w-96 flex-auto rounded-md hover:bg-blue-600 disabled:bg-blue-300" id="pay-btn">Pay</button>
    </div>
  </div>
  <script type="text/javascript">
    const stripe = Stripe('pk_test_51OQp7LKum5vhZVP1cxS9jisyC8zNoRnzmNtkpZ2NED4l9MZ05p5s5S4167FM7IiCXa3Ti3UTeWKbr0ohyl6Zrt7c00A8XGFtiy');

    const elements = stripe.elements({
      clientSecret: "<?php echo $_GET['sk'] ?>",
    });

    const paymentElement = elements.create('payment');
    paymentElement.mount('#payment-request-button');
    
    const addressElement = elements.create('address', { mode: 'billing' });
    addressElement.mount('#payment-contact-info');

    document.getElementById('pay-btn').addEventListener('click', async function (e) {
      this.disabled = true;
      const url = new URL(window.location.href);
      url.pathname = '/success.html'
      await stripe.confirmPayment({
        elements,
        confirmParams: {
          // Return URL where the customer should be redirected after the PaymentIntent is confirmed.
          return_url: url.href,
        },
      })
      .then(function(result) {
        if (result.error) {
          // Inform the customer that there was an error.
          alert(result.error.message);
        }
        this.disabled = false;
      })
    })
  </script>

</body>
</html>