<?php 
require_once("../vendor/autoload.php");
// include_once("payment.html");

if(isset($_POST["price"])) {
  // $email = $_POST["email"];
  // $phone_number = $_POST["phone_number"];
  $price = $_POST["price"];
  if(empty($price)) {
    die;
  }
  $stripe = new \Stripe\StripeClient('sk_test_51OQp7LKum5vhZVP1DikUKR2ET0JBirirnu6uS6XXCaJEYCpVCOCXfoQQS3ulKqB3VR4PK9oIPMTyQz3KzGE2IM5X00scxZSeK2');
  $paymentIntent = $stripe->paymentIntents->create([
    'amount' => $price * 100,
    'currency' => 'usd',
    'automatic_payment_methods' => ['enabled' => true],
    'setup_future_usage' => "on_session",
  ]);

  header("Location: /payment.php?sk=".$paymentIntent["client_secret"]);
  die;
}

else {
  include_once("form.html");
}


?>